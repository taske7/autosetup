## Description

Few semi-automated scripts to install all my preferred applications and copy over configurations and dotfiles from other repositories to a blank Manjaro VM installation.

## Installed

* Fonts:
    * ttf-ubuntu-font-family
    * ttf-fira-code

* Programs:
    * vscodium-bin
    * jetbrains-toolbox
    * google-chrome
    * vim
    * albert-lite
    * neofetch
    * tree
    * conky
    * curl
    * wget
    * git
    * npm
    * yay
    * plank
    * pywal

* Packages:
    * go
    * nodejs
    * jdk8-openjdk
    * java8-openjfx
    
* Cosmetic:
    * xcursor-breeze
    * matcha-gtk-theme
    * papirus-icon-theme